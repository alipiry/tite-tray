export const mainWindowConfig = {
  width: 300,
  height: 450,
  show: false,
  frame: false,
  fullscreenable: false,
  resizable: false,
  transparent: true,
  webPreferences: {
    backgroundThrottling: false
  }
};

export const trayConfig = {
  iconName: 'icon.png'
};
