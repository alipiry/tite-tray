import { app, BrowserWindow, Tray, ipcMain, nativeImage } from 'electron';
import * as path from 'path';
import { trayConfig, mainWindowConfig } from './config';

type pose = {
  x: number,
  y: number
};

const srcPath: any = path.join(__dirname, 'src');
const iconPath: any = path.join(__dirname, 'src/assets/icon.png');
let tray: Tray = null;
let window: BrowserWindow = null;

app.on('ready', () => {
  app.dock.hide();
  createTray();
  createWindow();
});

const createTray = (): void => {
  tray = new Tray(nativeImage.createFromPath(iconPath));
  tray.on('right-click', toggleWindow);
  tray.on('double-click', toggleWindow);
  tray.on('click', event => toggleWindow());
};

const createWindow = (): void => {
  window = new BrowserWindow(mainWindowConfig);
  window.loadURL(`file://${path.join(srcPath, 'index.html')}`);

  window.on('blur', () => {
    if (!window.webContents.isDevToolsOpened()) {
      window.hide();
    }
  });
};

const getWindowPosition = (): pose => {
  const x = Math.round(tray.getBounds().x + (tray.getBounds().width / 2) - (window.getBounds().width / 2));
  const y = Math.round(tray.getBounds().y + tray.getBounds().height + 4);
  return { x, y };
};

const toggleWindow = (): void => {
  if (window.isVisible()) {
    window.hide();
  }

  showWindow();
};

const showWindow = (): void => {
  window.setPosition(getWindowPosition().x, getWindowPosition().y, false);
  window.show();
  window.focus();
};

ipcMain.on('show-window', () => showWindow());
