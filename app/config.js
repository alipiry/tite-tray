"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mainWindowConfig = {
    width: 300,
    height: 450,
    show: false,
    frame: false,
    fullscreenable: false,
    resizable: false,
    transparent: true,
    webPreferences: {
        backgroundThrottling: false
    }
};
exports.trayConfig = {
    iconName: 'icon.png'
};
//# sourceMappingURL=config.js.map