"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const path = require("path");
const config_1 = require("./config");
const srcPath = path.join(__dirname, 'src');
const iconPath = path.join(__dirname, 'src/assets/icon.png');
let tray = null;
let window = null;
electron_1.app.on('ready', () => {
    electron_1.app.dock.hide();
    createTray();
    createWindow();
});
const createTray = () => {
    tray = new electron_1.Tray(electron_1.nativeImage.createFromPath(iconPath));
    tray.on('right-click', toggleWindow);
    tray.on('double-click', toggleWindow);
    tray.on('click', event => toggleWindow());
};
const createWindow = () => {
    window = new electron_1.BrowserWindow(config_1.mainWindowConfig);
    window.loadURL(`file://${path.join(srcPath, 'index.html')}`);
    window.on('blur', () => {
        if (!window.webContents.isDevToolsOpened()) {
            window.hide();
        }
    });
};
const getWindowPosition = () => {
    const x = Math.round(tray.getBounds().x + (tray.getBounds().width / 2) - (window.getBounds().width / 2));
    const y = Math.round(tray.getBounds().y + tray.getBounds().height + 4);
    return { x, y };
};
const toggleWindow = () => {
    if (window.isVisible()) {
        window.hide();
    }
    showWindow();
};
const showWindow = () => {
    window.setPosition(getWindowPosition().x, getWindowPosition().y, false);
    window.show();
    window.focus();
};
electron_1.ipcMain.on('show-window', () => showWindow());
//# sourceMappingURL=main.js.map